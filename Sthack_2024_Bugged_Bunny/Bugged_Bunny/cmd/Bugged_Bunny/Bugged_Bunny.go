package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"

	"github.com/gorilla/mux"

	utils "Bugged_Bunny/cmd/Bugged_Bunny/app"

	sf "Bugged_Bunny/pkg/syscall_finder"
)

func main() {
	muxRouter := mux.NewRouter()

	muxRouter.HandleFunc("/-_-...", Well)
	muxRouter.HandleFunc("/Noice!", Noice)
	muxRouter.HandleFunc("/Toobig.html", toobig)
	muxRouter.HandleFunc("/?Noice?", NotSoNoice)
	muxRouter.HandleFunc("/upload", fileHandler)
	muxRouter.HandleFunc("/runtime.go", runtime)

	muxRouter.PathPrefix("/").Handler(http.StripPrefix("/", http.FileServer(http.Dir("./front/"))))

	logHandler := utils.LogRequestHandler(muxRouter)

	server := &http.Server{Addr: ":80", Handler: logHandler, ErrorLog: log.New(io.Discard, "", 0)}
	fmt.Println("Serving on port :80")
	err := server.ListenAndServe()
	fmt.Println(err)

}

func toobig(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./front/TooBig.html")
}

func runtime(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "../crun/cmd/crun/crun.go")
}

func Well(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./front/-_-.html")
}

func Noice(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./front/Noice.html")
}

func NotSoNoice(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, "./front/NotSoNoice.html")
}

func fileHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		http.Error(w, "Method not Allowed", http.StatusMethodNotAllowed)
		return
	}

	r.Body = http.MaxBytesReader(w, r.Body, 1024*1024*50)
	err := r.ParseMultipartForm(1024 * 1024 * 50)
	if err != nil {
		http.Redirect(w, r, "/TooBig.html", http.StatusTemporaryRedirect)
		return
	}

	f, _, err := r.FormFile("file")
	if err != nil {
		// File not submitted? Handle error
		http.Error(w, "You must upload a file", http.StatusBadRequest)
		return
	}

	// Save it:
	saveName := "/tmp/uploads/" + utils.GenerateRandomString(12)
	os.Mkdir("/tmp/uploads", 0700)
	savef, err := os.Create(saveName)
	if err != nil {
		// Failed to create file on server, handle err
		http.Error(w, "Failed to save file", http.StatusInternalServerError)
		return
	}
	defer savef.Close()

	io.Copy(savef, f)

	isSafe, _ := sf.IsBinarySafe(saveName)
	if isSafe {
		cmd := exec.Command("/bin/bash", "-c", "cd ../crun; /usr/local/go/bin/go run cmd/crun/crun.go run "+saveName)
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		go cmd.Run()

		http.Redirect(w, r, "/Noice!", http.StatusTemporaryRedirect)
		return
	}

	http.Redirect(w, r, "/-_-...", http.StatusTemporaryRedirect)
	return
}
