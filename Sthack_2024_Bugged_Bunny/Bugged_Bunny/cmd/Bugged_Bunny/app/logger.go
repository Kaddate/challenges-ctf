package app

import (
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// LogReqInfo describes info about HTTP request
type HTTPReqInfo struct {
	method     string
	uri        string
	referer    string
	remoteAddr string
	statusCode int
	userAgent  string
}

type loggingResponserWriter struct {
	http.ResponseWriter
	statusCode int
}

// request logger handler
func LogRequestHandler(h http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		ri := &HTTPReqInfo{
			method:    r.Method,
			uri:       r.URL.String(),
			referer:   r.Header.Get("Referer"),
			userAgent: r.Header.Get("User-Agent"),
		}
		lrw := &loggingResponserWriter{w, http.StatusOK}
		h.ServeHTTP(lrw, r)

		ri.statusCode = lrw.statusCode
		ri.remoteAddr = r.RemoteAddr
		logRequest(ri)
	}
	return http.HandlerFunc(fn)
}

func logRequest(ri *HTTPReqInfo) {
	var rec []string
	rec = append(rec, strconv.Itoa(int(time.Now().UnixMilli())))
	rec = append(rec, "http")
	rec = append(rec, ri.method)
	rec = append(rec, ri.uri)
	if ri.referer != "" {
		rec = append(rec, ri.referer)
	}
	rec = append(rec, ri.remoteAddr)
	rec = append(rec, strconv.Itoa(ri.statusCode))
	rec = append(rec, ri.userAgent)

	writelog(rec)
}

func writelog(data []string) {
	concatData := strings.Join(data, "  ")
	os.Mkdir("/tmp/bunny_logs_please_dont_touch_uwu", 0700)
	logFile := "/tmp/bunny_logs_please_dont_touch_uwu/bunny.log"
	f, err := os.OpenFile(logFile, os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	if _, err = f.WriteString(concatData + "\n"); err != nil {
		panic(err)
	}
}
