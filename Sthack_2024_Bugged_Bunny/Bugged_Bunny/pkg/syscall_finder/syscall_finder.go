package syscall_finder

import (
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

func IsBinarySafe(filepath string) (bool, error) {
	if !isCorrectArchitecture(filepath) {
		return false, fmt.Errorf("The program must be compiled for x86_64 or x86 architecture")
	}

	cmd := exec.Command(
		"/usr/bin/objdump",
		"-M",
		"intel",
		"--no-addresses",
		"--no-show-raw-insn",
		"-d",
		filepath,
	)
	output := must(cmd.Output())
	syscallsNumbers := getSyscalls(string(output))

	syscallNames := must(syscallNumbersToSyscallNames(syscallsNumbers))
	allowedSyscalls := []string{"read", "write"}
	if isProgramAllowed(syscallNames, allowedSyscalls...) {
		return true, nil
	} else {
		return false, fmt.Errorf("The program has been judged dangerous")
	}
}

func must[T any](obj T, err error) T {
	if err != nil {
		panic(err)
	}
	return obj
}

func getSyscalls(asmDump string) []int {
	var syscalls []int
	arrayDump := strings.Split(asmDump, "\n")
	for i, c := range arrayDump {
		if strings.Contains(c, "syscall") || strings.Contains(c, "int 0x80") {
			for sp := i; ; sp-- {
				if strings.Contains(arrayDump[sp], "mov") &&
					(strings.Contains(arrayDump[sp], "rax") || strings.Contains(arrayDump[sp], "eax") ||
						strings.Contains(arrayDump[sp], "ax") || strings.Contains(arrayDump[sp], "al")) {
					trimmed := strings.Trim(arrayDump[sp], "\t")
					afterMov := strings.Split(trimmed, "mov")[1]
					syscallNumber := strings.Split(afterMov, ",")[1]

					syscallNumberAsDecimal, _ := strconv.ParseInt(syscallNumber[2:], 16, 32)

					syscalls = append(syscalls, int(syscallNumberAsDecimal))
					break
				}
			}
		}
	}
	return syscalls
}

func syscallNumbersToSyscallNames(numbers []int) ([]string, error) {
	// using /usr/include/asm/unistd_64.h as reference, since this program is meant to run on amd64 architectures
	// body := must(os.ReadFile("/usr/include/asm/unistd_64.h")) // Arch
	body := must(os.ReadFile("/usr/include/asm-generic/unistd.h")) // Ubuntu

	lines := strings.Split(string(body), "\n")

	var syscallNames []string
	for i := range numbers {
		// foundCount := len(syscallNames)
		for _, l := range lines {
			if l == "" {
				continue
			}

			split := strings.Split(l, " ")

			if len(split) <= 2 {
				continue
			}

			if !strings.Contains(split[1], "__NR_") {
				continue
			}

			number, err := strconv.Atoi(split[2])
			if err != nil {
				continue
			}

			if number == numbers[i] {
				// removes "__NR_" from syscall name
				realSyscallName := split[1][5:]
				syscallNames = append(syscallNames, realSyscallName)
				break
			}
		}
		//if len(syscallNames) == foundCount {
		//	return nil, fmt.Errorf("No syscall found for syscall number %d", numbers[i])
		//}
	}
	return syscallNames, nil
}

func isProgramAllowed(syscalls []string, allowedSyscalls ...string) bool {
	for _, syscall := range syscalls {
		var allowed bool
		for _, allowedSyscall := range allowedSyscalls {
			if syscall == allowedSyscall {
				allowed = true
				continue
			}
		}
		if !allowed {
			return false
		}
	}
	return true
}

func isCorrectArchitecture(filepath string) bool {
	// Open the file
	file := must(os.Open(filepath))
	defer file.Close()

	// Define the offset and the number of bytes to read
	offset := int64(0x12)
	numBytes := 2

	// Seek to the specified offset
	must(file.Seek(offset, 0))

	// Read the bytes
	buf := make([]byte, numBytes)
	must(file.Read(buf))

	x86 := byte(0x3e)
	x86_64 := byte(0x03)

	if buf[0] == x86 || buf[0] == x86_64 {
		return true
	}

	return false
}
