package main

import (
	"fmt"
	"os"
	"os/exec"
	"path"
	"syscall"

	utils "crun/cmd/crun/app"
)

func main() {
	switch os.Args[1] {
	case "run":
		parent()
	case "child":
		child()
	default:
		panic("must be run or child")
	}
}

func parent() {
	cmd := exec.Command("/proc/self/exe", append([]string{"child"}, os.Args[2:]...)...)
	cmd.SysProcAttr = &syscall.SysProcAttr{
		Cloneflags: syscall.CLONE_NEWUTS | syscall.CLONE_NEWPID | syscall.CLONE_NEWNS,
	}
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		fmt.Println("End of command")
		os.Exit(1)
	}
}

func child() {
	containerPath := "/tmp/" + utils.GenerateRandomString(20)

	// copy the busybox fs to the container directory
	utils.Must(utils.CopyDir("./rootfs", containerPath))
	utils.Must(os.Mkdir(containerPath+"/proc", 0700))

	// copy the binary to the container fs
	binaryName := path.Base(os.Args[2])
	os.Chmod(os.Args[2], 0700)
	utils.Must(utils.CopyFile(os.Args[2], containerPath+"/"+binaryName))

	// Chroot the container
	utils.Must(syscall.Chroot(containerPath))
	utils.Must(os.Chdir("/"))

	// Mount the proc filesystem in the new root
	utils.Must(syscall.Mount("proc", "/proc", "proc", 0, ""))

	// exec the binary until death
	cmd := exec.Command("/"+binaryName, os.Args[3:]...)
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Run(); err != nil {
		utils.CleanContainerEnvironment(containerPath)
		os.Exit(0)
	}
}
