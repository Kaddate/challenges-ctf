#!/bin/bash

# Define the character set
charset='abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'


# Find directories in /tmp matching the pattern and modified more than 12 minutes ago
find /tmp -maxdepth 1 -mindepth 1 -type d -name "[$charset]*" -mmin +12 | while read -r dir; do
    # Check if the directory name is exactly 20 characters long
    if [[ $(basename "$dir") =~ ^[$charset]{20}$ ]]; then
        # Check if /proc is mounted in this directory
        mountpoint="$dir/proc"
        if mountpoint -q "$mountpoint"; then
            # Unmount the proc filesystem
            umount "$mountpoint"
        fi
        # Delete the directory
        rm -rf "$dir"
    fi
done
