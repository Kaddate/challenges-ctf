# Before running anything, you'll have to download busybox container image and extract it, as gitlab as a limit on file storage :

```bash
wget https://github.com/docker-library/busybox/raw/333060c8efc4ce573d8cf2a2b662d706d4b93f83/latest/uclibc/amd64/rootfs.tar.gz -o ./crun/rootfs/rootfs.tar.gz
tar zxvf ./crun/rootfs/rootfs.tar.gz
```

# Run the chall :

```bash
cd Bugged_Bunny
sudo go run cmd/Bugged_Bunny/Bugged_Bunny.go
```