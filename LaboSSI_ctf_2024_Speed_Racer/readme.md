# Speed Racer

## build

```bash
docker build . -t speed_racer
```

## Run

```bash
docker run -d -p 80:8080 speed_racer
```
