package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/exec"
)

const uploadsDir = "./uploads/"
const NotScannedFile = "scanMe.go"
const ScannedFile = "executeMe.go"

func main() {
	http.HandleFunc("/", redirect)
	http.HandleFunc("/index.html", index)
	http.HandleFunc("/send", send)

	http.ListenAndServe(":8080", nil)
}

func redirect(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "/index.html", http.StatusPermanentRedirect)
}

func index(w http.ResponseWriter, r *http.Request) {
	file, err := os.Open(r.URL.Path[1:])
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	defer file.Close()
	fileInfo, err := file.Stat()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	http.ServeContent(w, r, file.Name(), fileInfo.ModTime(), file)
}

func send(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte("403 - Only POST allowed!"))
		return
	}

	if err := r.ParseForm(); err != nil {
		fmt.Fprintf(w, "ParseForm() err: %v", err)
		return
	}
	URL := r.FormValue("URL")

	if err := inspectFile(URL); err != nil {
		w.WriteHeader(http.StatusForbidden)
		w.Write([]byte(fmt.Sprintf("<h1>The server returned the following error :</h1>\n\t<h2>%s</h2>", err.Error())))
		return
	}
	w.Write([]byte(fmt.Sprint("<h1>Your program has been executed on our very secure server !<h1>")))
	executeFile(URL)
}

func inspectFile(url string) (err error) {
	err = downloadFile(url, "file-auditor", NotScannedFile)
	defer os.Remove(uploadsDir + NotScannedFile)
	if err != nil {
		return
	}

	cmd := exec.Command("/go/bin/static_scanner", "-fmt=json", "-out=./scans/scan.json", uploadsDir)
	cmd.Run()

	if err = getStaticScannerOutput("./scans/scan.json"); err != nil {
		return err
	}
	return
}

// downloadFile downloads a file using the specified URL and user-agent via the http or https protocol.
// It then inserts the file into the /uploads/ directory with the specified file name.
func downloadFile(url, ua, fileName string) (err error) {
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return fmt.Errorf("Could not fetch the provided URL...")
	}
	req.Header.Set("User-Agent", ua)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return fmt.Errorf("downloadFile err : Si cette erreur persiste, contacte Kaddate...")
	}
	defer resp.Body.Close()

	content, err := io.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("downloadFile err : Si cette erreur persiste, contacte Kaddate...")
	}

	if err = os.WriteFile(uploadsDir+fileName, content, 0600); err != nil {
		return fmt.Errorf("downloadFile err : Si cette erreur persiste, contacte Kaddate...")
	}
	fmt.Println(string(content))
	return nil
}

func executeFile(url string) {
	err := downloadFile(url, "file-executor", ScannedFile)
	defer os.Remove(uploadsDir + ScannedFile)
	if err != nil {
		return
	}

	cmd := exec.Command("go", "run", uploadsDir+ScannedFile)
	cmd.Run()
}

// getStaticScannerOutput only fetches the result of static_scanner scan, if the number of vulnerabilites is >= 1
// it returns an error
// This function might looks complicated, but it just only parses a json file. Nothing to see here...
// Really...
func getStaticScannerOutput(path string) (err error) {
	jsonFile, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("getStaticScannerOutput err : Si cette erreur persiste, contacte Kaddate...")
	}
	jsonData, err := io.ReadAll(jsonFile)
	if err != nil {
		return
	}
	jsonFile.Close()

	// check if file is empty
	if len(jsonData) == 0 {
		return fmt.Errorf("getStaticScannerOutput err : Si cette erreur persiste, contacte Kaddate...")
	}

	var data map[string]interface{}
	err = json.Unmarshal([]byte(jsonData), &data)
	if err != nil {
		return fmt.Errorf("getStaticScannerOutput err : Si cette erreur persiste, contacte Kaddate...")
	}

	stats, found := data["Stats"].(map[string]interface{})
	if !found {
		return fmt.Errorf("getStaticScannerOutput err : Si cette erreur persiste, contacte Kaddate...")
	}

	foundValue, found := stats["found"].(float64)
	if !found {
		fmt.Println(foundValue)
		return fmt.Errorf("getStaticScannerOutput err : Si cette erreur persiste, contacte Kaddate...")
	}

	fmt.Println(foundValue)
	if foundValue != 0 {
		return fmt.Errorf("Found %g insecure snippets in the given script", foundValue)
	}

	return nil
}
