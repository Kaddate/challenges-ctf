package main

import (
	"log"
	"net/http"
	"os"
)

var APP_ROOT = os.Getenv("APP_ROOT")
var FRONT_OFFICE_ROOT = APP_ROOT + "/front-ui"

// Création de route pour chaque page web afin d'effectuer chaqu'une fonctions
func main() {
	index := http.FileServer(http.Dir(FRONT_OFFICE_ROOT))
	http.Handle("/", http.StripPrefix("/", index))

	log.Fatal(http.ListenAndServe(":80", nil))
}
