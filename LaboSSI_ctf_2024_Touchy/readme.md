# Intitulé

```plain
/!\ AUCUN DOCKER ESCAPE N'EST NÉCESSAIRE / TOLÉRÉ /!\

Vous êtes missionés pour faire un pentest sur notre site vitrine. Il dispose d'un back et d'un front office. Les accès au back office sont filtrés pour nous assurer que des personnes extérieures ne puissent se connecter dessus. Vous ferez les tests sur un environement de développement, il est donc possible qu'il y ait du traffic sur l'application. Mais pas d'inquiétude ! Vous pourrez faire tous les tests que vous voulez.

À noter que l'environemment à disposition est une copie de notre environnement de production.
```

## Run

```bash
# Build & Run with verbose
cp index.html.bak ./front-ui/index.html; docker compose build; docker-compose --verbose up
# Build & Run Daemonized
cp index.html.bak ./front-ui/index.html; docker compose build; docker-compose up -d
```