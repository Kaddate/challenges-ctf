module touchy

go 1.21.4

require (
	github.com/gorilla/mux v1.8.1
	github.com/mattn/go-sqlite3 v1.14.19
	golang.org/x/crypto v0.18.0
)
