package main

import (
	"errors"
	"fmt"
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"time"
)

func checkAuthParameters(w http.ResponseWriter, r *http.Request) (username, password string, err error) {
	username = r.FormValue("username")
	password = r.FormValue("password")
	if username == "" || password == "" {
		return "", "", errors.New("Empty Fields !")
	}
	return username, password, err
}

func serveTemplate(w http.ResponseWriter, message, file string) {
	t, err := template.New("Template").ParseFiles(BACK_OFFICE_ROOT+file, BACK_OFFICE_ROOT+"/error-message.html", BACK_OFFICE_ROOT+"/success-message.html")
	if err != nil {
		log.Fatalln(err.Error())
	}

	err = t.ExecuteTemplate(w, "base", message)
	if err != nil {
		fmt.Println(err)
	}
}

func generateRandomString(length int) string {
	const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	var seededRand *rand.Rand = rand.New(rand.NewSource(time.Now().UnixNano()))
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

func isAdmin(r *http.Request) bool {
	cookie, err := r.Cookie("Session")
	if err != nil {
		return false
	}

	role, err := getRoleFromCookie(cookie.Value)
	if err != nil {
		return false
	}
	if role == "admin" {
		return true
	}
	return false
}
