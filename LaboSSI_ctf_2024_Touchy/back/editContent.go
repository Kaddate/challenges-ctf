package main

import (
	"html/template"
	"log"
	"net/http"
	"os"
)

func EditContent(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		// serveFile("/admin/editContent.html", w, r)
		serveEditContent(w)
		return
	} else if r.Method != "POST" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}

	if !isAdmin(r) {
		http.Error(w, "You must be admin to make this action !", http.StatusUnauthorized)
		return
	}

	content := r.FormValue("content")
	if content == "" {
		http.Error(w, "Empty Fields !", http.StatusBadRequest)
		return
	}

	err := os.WriteFile(APP_ROOT+"/front-ui/index.html", []byte(content), 0666)
	if err != nil {
		log.Fatalln(err.Error())
	}

	// serveFile("/admin/editContent.html", w, r)
	serveEditContent(w)
	return
}

func serveEditContent(w http.ResponseWriter) {
	body, err := os.ReadFile(APP_ROOT + "/front-ui/index.html")
	if err != nil {
		log.Fatalln(err.Error())
	}

	t, err := template.New("editContent").ParseFiles(BACK_OFFICE_ROOT + "/admin/editContent.html")
	err = t.ExecuteTemplate(w, "T", string(body))
}
