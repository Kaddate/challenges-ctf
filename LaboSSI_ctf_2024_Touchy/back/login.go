package main

import (
	"net/http"
)

func Login(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		serveTemplate(w, "", "/login.html")
		return
	} else if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}

	username, password, err := checkAuthParameters(w, r)
	if err != nil {
		serveTemplate(w, err.Error(), "/login.html")
		return
	}
	if !authenticateUser(username, password) {
		serveTemplate(w, "Wrong Username or Pasword", "/login.html")

		return
	}

	cookieValue := hashUserPassword(generateRandomString(10) + username + generateRandomString(10))
	if err = addCookieToDatabase(cookieValue, isAdminInDatabase(username)); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	cookie := http.Cookie{
		Name:     "Session",
		Value:    cookieValue,
		Path:     "/",
		MaxAge:   3600,
		HttpOnly: true,
		Domain:   "touchy.fr",
	}

	http.SetCookie(w, &cookie)
	http.Redirect(w, r, "/admin", 301)
	return
}
