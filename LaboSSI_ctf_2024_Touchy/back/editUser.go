package main

import (
	"net/http"
)

func EditUser(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		serveTemplate(w, "", "/admin/editUser.html")
		return
	} else if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}

	if !isAdmin(r) {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("You must be admin to make this action !"))
		return
	}

	username := r.FormValue("username")
	role := r.FormValue("role")
	if username == "" || role == "" {
		serveTemplate(w, "Empty Fields !", "/admin/editUser.html")
		return
	}
	if !isUser(username) {
		serveTemplate(w, "User not found !", "/admin/editUser.html")
		return
	}

	err := updateUserInDatabase(username, role)
	if err != nil {
		serveTemplate(w, "Role must be 'admin' or 'rédacteur' !", "/admin/editUser.html")
		return
	}

	serveTemplate(w, "Action performed", "/admin/editUser.html")
}
