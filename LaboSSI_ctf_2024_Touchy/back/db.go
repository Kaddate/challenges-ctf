package main

import (
	"database/sql"
	"errors"
	"fmt"
	"log"

	_ "github.com/mattn/go-sqlite3"
	bcrypt "golang.org/x/crypto/bcrypt"
)

func newDatabaseDriver() *sql.DB {
	databaseDriver, err := sql.Open("sqlite3", APP_ROOT+"/database.db")
	if err != nil {
		log.Fatal(err.Error())
	}
	return databaseDriver
}

func AddUserToDatabase(username, password, role string) error {
	insertUserStatement := `INSERT INTO users (Username, Password, Role) VALUES (
		?, ?, ?
	);`

	prepareStatement, err := databaseDriver.Prepare(insertUserStatement)
	if err != nil {
		log.Fatal(err.Error())
	}

	_, err = prepareStatement.Exec(username, hashUserPassword(password), role)
	if err != nil {
		return err
	}
	return nil
}

func updateUserInDatabase(username, newRole string) error {
	updateUserStatement := `UPDATE users SET Role = ? WHERE Username = ?;`

	prepareStatement, err := databaseDriver.Prepare(updateUserStatement)
	if err != nil {
		log.Fatal(err.Error())
	}

	_, err = prepareStatement.Exec(newRole, username)
	if err != nil {
		return err
	}
	return nil
}

func addCookieToDatabase(cookie string, isAdmin bool) error {
	insertUserStatement := `INSERT INTO cookies (Cookie, Role) VALUES (
		?, ?
	);`

	prepareStatement, err := databaseDriver.Prepare(insertUserStatement)
	if err != nil {
		log.Fatal(err.Error())
	}

	var role string
	if isAdmin {
		role = "admin"
	} else {
		role = "rédacteur"
	}

	_, err = prepareStatement.Exec(cookie, role)
	if err != nil {
		return err
	}
	return nil
}

func getRoleFromCookie(cookie string) (string, error) {
	selectStatement := `SELECT Role FROM cookies WHERE Cookie = ?;`

	stm, err := databaseDriver.Prepare(selectStatement)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer stm.Close()

	var role string
	err = stm.QueryRow(cookie).Scan(&role)
	if err != nil {
		if err == sql.ErrNoRows {
			return "", errors.New("Not connected")
		}
		return "", err
	}
	return role, nil
}

func authenticateUser(username, password string) bool {
	selectStatement := `SELECT Username, Password FROM users WHERE Username = ?`
	stm, err := databaseDriver.Prepare(selectStatement)
	if err != nil {
		log.Fatal(err)
	}
	defer stm.Close()

	var Username string
	var PasswordHash string
	err = stm.QueryRow(username).Scan(&Username, &PasswordHash)
	if err != nil {
		if err == sql.ErrNoRows {
			fmt.Println("User not found.")
			return false
		} else {
			log.Fatal(err)
		}
	}

	if err = bcrypt.CompareHashAndPassword([]byte(PasswordHash), []byte(password)); err == nil {
		return true
	}
	return false
}

func isAdminInDatabase(username string) bool {
	selectStatement := `SELECT Role FROM users WHERE Username = ?`
	stm, err := databaseDriver.Prepare(selectStatement)
	if err != nil {
		log.Fatal(err)
	}
	defer stm.Close()

	var Role string
	err = stm.QueryRow(username).Scan(&Role)
	if err != nil {
		if err == sql.ErrNoRows {
			return false
		} else {
			log.Fatal(err)
		}
	}

	if Role == "admin" {
		return true
	}
	return false
}

func isUser(username string) bool {
	selectStatement := `SELECT Username FROM users WHERE Username = ?`
	stm, err := databaseDriver.Prepare(selectStatement)
	if err != nil {
		log.Fatal(err)
	}
	defer stm.Close()

	var User string
	err = stm.QueryRow(username).Scan(&User)
	if err != nil {
		if err == sql.ErrNoRows {
			return false
		} else {
			log.Fatal(err)
		}
	}
	return true
}

func hashUserPassword(p string) string {
	bytes, _ := bcrypt.GenerateFromPassword([]byte(p), 14)
	return string(bytes)
}
