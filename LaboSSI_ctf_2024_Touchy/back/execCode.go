package main

import (
	"log"
	"net/http"
	"os"
	"os/exec"
)

func ExecCode(w http.ResponseWriter, r *http.Request) {
	if !isAdmin(r) {
		w.WriteHeader(http.StatusUnauthorized)
		w.Write([]byte("You must be admin to make this action !"))
		return
	}

	if r.Method == "GET" {
		serveTemplate(w, "", "/admin/execCode.html")
		return
	} else if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}

	content := r.FormValue("content")
	if content == "" {
		serveTemplate(w, "Empty Fields !", "/admin/execCode.html")
		return
	}

	if err := saveContentToFile(content); err != nil {
		log.Fatalln(err.Error())
		return
	}

	execCodeFromFile()
	serveTemplate(w, "", "/admin/execCode.html")
}

func saveContentToFile(content string) (err error) {
	f, err := os.Create(APP_ROOT + "/execCode.go")
	if err != nil {
		log.Fatal(err)
	}
	f.Close()

	if err = os.WriteFile(APP_ROOT+"/execCode.go", []byte(content), 0600); err != nil {
		return err
	}
	return nil
}

func execCodeFromFile() {
	cmd := exec.Command("/usr/local/go/bin/go", "run", APP_ROOT+"/execCode.go")
	cmd.Run()
}
