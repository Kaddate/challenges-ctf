package main

import (
	"database/sql"
	"log"
	"os"

	_ "github.com/mattn/go-sqlite3"
)

func InstantiateDatabase() {
	createDatabaseFile()
	databaseDriver = newDatabaseDriver()

	createUserTable(databaseDriver)
	createCookieTable(databaseDriver)
	populateUserTable(databaseDriver)
	populatCookieTable(databaseDriver)
}

func createDatabaseFile() {
	f, err := os.Create(APP_ROOT + "/database.db")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
}

func createUserTable(db *sql.DB) {
	userTable := `CREATE TABLE IF NOT EXISTS users (
		"Id" INTEGER NOT NULL PRIMARY KEY,
		"Username" VARCHAR(100) UNIQUE NOT NULL,
		"Password" VARCHAR(255) UNIQUE NOT NULL,
		"Role" TEXT CHECK ("Role" IN ('rédacteur', 'admin'))
	);`

	statement, err := db.Prepare(userTable)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec()
	log.Println("Users table correctly created")
}

func createCookieTable(db *sql.DB) {
	cookieTable := `CREATE TABLE IF NOT EXISTS cookies (
		"Cookie" VARCHAR(20) UNIQUE NOT NULL,
		"Role" TEXT CHECK ("Role" IN ('rédacteur', 'admin'))
	);`

	statement, err := db.Prepare(cookieTable)
	if err != nil {
		log.Fatal(err.Error())
	}
	statement.Exec()
	log.Println("Cookies table correctly created")
}

func populateUserTable(db *sql.DB) {
	AddUserToDatabase("admin", "ExtremelyStrongPassword123!__987654321", "admin")
	log.Println("Users table correctly populated")
}

func populatCookieTable(db *sql.DB) {
	addCookieToDatabase("VerySecureAdminCookie", true)
	log.Println("Cookies table correctly populated")
}
