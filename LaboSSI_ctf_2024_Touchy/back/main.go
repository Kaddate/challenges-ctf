package main

import (
	"database/sql"
	"net/http"
	"os"

	"github.com/gorilla/mux"

	_ "github.com/mattn/go-sqlite3"
)

var APP_ROOT = os.Getenv("APP_ROOT")
var BACK_OFFICE_ROOT = APP_ROOT + "/back-ui"
var databaseDriver *sql.DB

func main() {
	InstantiateDatabase()
	databaseDriver = newDatabaseDriver()

	muxRouter := mux.NewRouter()
	muxRouter.HandleFunc("/admin", admin)
	muxRouter.HandleFunc("/login", Login)
	muxRouter.HandleFunc("/register", Register)
	muxRouter.HandleFunc("/admin/editUser", EditUser)
	muxRouter.HandleFunc("/admin/editContent", EditContent)
	muxRouter.HandleFunc("/admin/execCode", ExecCode)

	styles := http.FileServer(http.Dir(BACK_OFFICE_ROOT + "/styles"))
	muxRouter.PathPrefix("/styles/").Handler(http.StripPrefix("/styles/", styles))

	server := &http.Server{Addr: ":80", Handler: muxRouter}
	server.ListenAndServe()
}

func admin(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, BACK_OFFICE_ROOT+"/admin/index.html")
}
