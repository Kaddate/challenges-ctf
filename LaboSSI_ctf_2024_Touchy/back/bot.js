var page = require('webpage').create();
var host = "front.touchy.fr";
var url = "http://"+host+"/index.html";
var timeout = 2000;
phantom.addCookie({
    'name': 'Session',
    'value': 'VerySecureAdminCookie',
    'domain': "touchy.fr",
    'path': '/',
    'httponly': true
});
page.onNavigationRequested = function(url, type, willNavigate, main) {
    console.log("[URL] URL="+url);  
};
page.settings.resourceTimeout = timeout;
page.onResourceTimeout = function(e) {
    setTimeout(function(){
        console.log("[INFO] Timeout")
        phantom.exit();
    }, 1);
};
page.open(url, function(status) {
    console.log("[INFO] rendered page");
    setTimeout(function(){
        phantom.exit();
    }, 1);
});
