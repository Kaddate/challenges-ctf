package main

import (
	"net/http"
)

func Register(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		serveTemplate(w, "", "/register.html")
		return
	} else if r.Method != "POST" {
		w.WriteHeader(http.StatusMethodNotAllowed)
		w.Write([]byte("Method not allowed"))
		return
	}

	username, password, err := checkAuthParameters(w, r)
	if err != nil {
		serveTemplate(w, err.Error(), "/register.html")
		return
	}

	if err := AddUserToDatabase(username, password, "rédacteur"); err != nil {
		serveTemplate(w, "User already exists.", "/register.html")
		return
	}
	serveTemplate(w, "Successfully Signed Up", "/register.html")
}
